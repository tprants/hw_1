public class ColorSort {

   enum Color {red, green, blue};
   
   public static void main (String[] param) {
      // for debugging
	  /* Color[] varvid= {Color.red , Color.green , Color.blue , Color.red , Color.green, Color.blue};
	   reorder(varvid);
	   for (int i = 0; i < varvid.length; i++) {
		   System.out.println(varvid[i]);
	   }*/
   }
   //red, green and blue,
   //organises into that order
   public static void reorder (Color[] balls) {
      // TODO!!! Your program here
	   int r = 0; //number of reds
	   int g = 0; //number of greens
	   int b = 0; //number of blues
	   for (int i = 0; i < balls.length; i++) {
		   if (balls[i]==Color.red) {
			   r++;
		   }
		   else if (balls[i]==Color.green) {
			   g++;
		   }
		   else {
			   b++;
		   }
	   }
	   //program knows how many colors are there now
	   for (int i = 0; i < balls.length; i++) {
		   if (r > 0) {
			   balls[i] = Color.red;
			   r--;
		   }
		   else if (g > 0) {
			   balls[i] = Color.green;
			   g--;
		   }
		   else {
			   balls[i] = Color.blue;
			   b--;
		   }
	   }
   }
}

